---
title: Start
tags:
date: 2016-09-27 15:13:39
---

První článek = nejhorší článek. Proto je tenhle vyplněn texty o&nbsp;ničem. Lorem ipsum?

<!-- more -->
Tak OK. Nevím, kam to poletí, ale už to startuje. Na začátku určitě přiletí pár článků o&nbsp;generátorech statických stránek, textových editorech a&nbsp;tak podobně. Možná nějaké srovnávací testy softwaru. Pak snad nějaké tipy a&nbsp;triky na Mac OS&nbsp;X, popřípadě i&nbsp;na iOS.

Díky za strpení.
