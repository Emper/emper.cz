---
title: "Atom 01: Vzhled, balíčky, zkratky"
tags: [ide, atom, workflow]
homepage: false
date: 2016-10-19 00:53:37
---
Atom je multiplatformní free open-source editor textu a&nbsp;zdrojového kódu. Toto vývojové prostředí je výtvor samotného GitHubu napsaný v&nbsp;javascriptu a&nbsp;postavený na Electronu.

<!-- more -->
[Editor Atom](https://atom.io) je zcela **zdarma**. Už to nám napovídá, že asi nenabídne tolik, co třeba profesionálnější ale zpoplatněný Sublime Text. I&nbsp;tak se ale Atom má čím chlubit a&nbsp;většinu konkurenčních free editorů strčí do kapsy. Instalaci a&nbsp;základní práci s editorem popisovat snad nemusím, proto přeskočím rovnou k personalizaci editoru.


## Vzhled
Vzhled vývojového prostředí (IDE) můžeme měnit v&nbsp;nastavení *Settings -> Themes* (otevřeme třeba přes klávesovou zkratku `Cmd + Shift + P`, do lišty napíšeme "theme" a&nbsp;vybereme *Setting View: Change Themes*). Můžeme měnit a&nbsp;libovolně kombinovat vzhled *UI Theme* a&nbsp;*Syntax Theme*. To první je vzhled aplikace, horních tabů (záložek?), bočního menu a&nbsp;spodní lišty. To druhé se vztahuje na barvy samotného textového editoru s kódem.

Nový vzhled nainstalujeme přes *Settings -> Install* (*Setting View: Install Packages and Themes*), napíšeme název šablony a&nbsp;nesmíme zapomenout zaškrtnout, že hledáme v&nbsp;"Themes" (viz. obrázek). Vybraný theme nainstalujeme a&nbsp;poté zvolíme v *Settings -> Themes*, změna se projeví do pár sekund.

![Atom: Settings -> Themes](http://emper.cz/img/blog/atom-themes.png "Settings -> Themes")

Všimněte si, že klávesová zkratka `Cmd + Shift + P` zobrazí lištu příkazů. Nazývá se Command Palette a&nbsp;doporučuje se používat ve většině IDE.


## Balíčky
Podobně jako v&nbsp;jiných editorech, do Atomu se instalují balíčky (**packages**), které mohou sloužit k nejrůznějším účelům, od podpory programovacích jazyků (syntax highlight, snippets) až po dodatečné síťové funkce aplikace (ftp, git).

Nainstalovat balíček v *Settings -> Install* už bychom měli umět, hlavně pokud jsme přečetli předchozí kapitolu Vzhled. Většina balíčků nabízí ve svém vlastním *Settings* popis chování a&nbsp;ovládání doplňku, dostupné příkazy, popřípadě klávesové zkratky. Místo toho zjevného jenom vypíšu seznam nejpoužívanějších, nejlepších a&nbsp;nebo alespoň užitečných doplňků.

- [**git-plus**](https://atom.io/packages/git-plus): verzování bez terminálu
- [**file-icons**](https://atom.io/packages/file-icons): ikony souborů podle typu souboru
- [**autocomplete-paths**](https://atom.io/packages/autocomplete-paths): doplňování cest
- [**pigments**](https://atom.io/packages/pigments): barevné barvy v&nbsp;kódu
- [**color-picker**](https://atom.io/packages/color-picker): interaktivní paleta barev
- [**project-manager**](https://atom.io/packages/project-manager): přístup k&nbsp;ostatním projektům
- [**wordpress-api**](https://atom.io/packages/wordpress-api): hromada pomůcek pro práci s&nbsp;WordPressem
- [**laravel**](https://atom.io/packages/laravel) + [**language-blade**](https://atom.io/packages/language-blade): pro vývoj v&nbsp;Laravelu
- [**linter-php**](https://atom.io/packages/linter-php): kontrola PHP
- [**linter-eslint**](https://atom.io/packages/linter-eslint): kontrola JavaScriptu
- [**emmet**](https://atom.io/packages/emmet): viz. [tenhle článek](http://emper.cz/blog/emmet-je-zaklad/)

Ukázka balíčků color-picker a&nbsp;pigments v&nbsp;akci:

![Atom: pigments + color-picker](http://emper.cz/img/blog/atom-colors.png "pigments + color-picker")

## Klávesové zkratky
Stejně jako každý profesionální nástroj, i&nbsp;Atom nabízí řadu klávesových zkratek a&nbsp;také možnost je přenastavovat a&nbsp;tvořit nové. Níže jest seznam často používaných zkratek na Mac OS:

- `Cmd + \`: schovat/ukázat boční lištu (souborový strom)
- `Cmd + P`: hledat a otevřít soubor
- `Cmd + F`: hledat v&nbsp;souboru
- `Cmd + Shift + F`: hledat v&nbsp;celém projektu
- `Cmd + Shift + P`: lišta funkcí a&nbsp;příkazů (Command Palette)
- `Cmd + D`: vybrat slovo
- `Cmd + I`: vybrat celý řádek
- `Ctrl + G`: jít na řádek
- `Ctrl + Shift + K`: smazat řádek
- `Cmd + Shift + D`: duplikovat řádek
- `Cmd + Ctrl + nahoru/dolů`: přesunout řádek nahoru/dolů
- `Cmd + K, pak šipka`: rozdělit panel do směru konkrétní šipky

Více najdete v&nbsp;nastavení klávesových zkratek *Settings -> Keybindings*, kde je odkaz na hezky zdokumentovaný "keymap file" *keymap.cson*, ve kterém můžeme navázat konkrétní událost na určité klávesové kombinace. Soubor se jinak nachází ve složce `~/.atom/`.

Vlastní zkratky definujeme v&nbsp;CoffeeScript-Object-Notation, což je prostě JSON pro CoffeeScript. Například pro navázání události `add-all-commit-and-push` z balíčku `git-plus` na klávesovou zkratku `Cmd + Shift + U` zapíšeme tento kousek kódu do souboru *keymap.cson*:

```
'.platform-darwin':
  'shift-cmd-U': 'git-plus:add-all-commit-and-push'
```

---

Užitečné: [Documentation](https://atom.io/docs), [Themes](https://atom.io/themes), [Creating a&nbsp;Theme](http://flight-manual.atom.io/hacking-atom/sections/creating-a-theme/), [Packages](https://atom.io/packages), [Atom Editor Cheat Sheet](https://blog.bugsnag.com/atom-editor-cheat-sheet/)
