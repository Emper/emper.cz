---
title: "Jekyll: O co jde?"
tags: [static, jekyll]
homepage: true
date: 2016-10-07 16:40:28
---
Žádná databáze, žádné PHP, minimální zátěž serveru a neprolomitelná obrana. Nebojte se generátorů statických stránek. Jako první ze série představím Jekyll, generátor napsaný v&nbsp;Ruby jedním ze zakladatelů GitHubu.

<!-- more -->
Jedná se o jednoduchý generátor, který už od začátku počítá s&nbsp;tím, že budete postupně přidávat články do blogu. Není to ale CMS, jedině tak jakýsi souborový CMS. Ideální pro osobní nebo firemní stránky, které toho moc nepotřebují.

## Co Jekyll dělá
[Jekyll](https://jekyllrb.com) vezme všechny připravené soubory, přeloží Markdown (nebo Textile), Liduid, Sass a&nbsp;další soubory a&nbsp;vygeneruje kompletní statické stránky připravené pro nahrání na web. Nebo stránky můžete nahrát na free hosting s GitHub Pages, s&nbsp;vlastní doménou a&nbsp;se vším všudy.

Navíc lze do Jekyllu díky [importu](http://import.jekyllrb.com) převést už existující stránky, pokud běží na některém podporovaných systémů (WordPress, Joomla, Drupal, Blogger nebo třeba Tumblr).

## Instalace
Jak už jsem zmínil, Jekyll je napsaný v&nbsp;Ruby, takže instalovat budeme přes [RubyGems](https://rubygems.org) package manager a [Bundler](http://bundler.io). Do terminálu:

```
~ $ gem install jekyll bundler
~ $ jekyll new jekyllsite
~ $ cd jekyllsite
~/jekyllsite $ bundle install
```

Nyní máme vše potřebné ve složce *jekyllsite*. Stačí už jen spustit Jekyll server:

```
~/jekyllsite $ bundle exec jekyll serve
```

Do konzole se vypíše, na které adrese server běží. Standartně na `localhost:4000`. Další příkaz vygeneruje finální stránky do složky *_site/*:

```
~/jekyllsite $ bundle exec jekyll build
```

I když v Jekyll dokumentaci stále píšou o&nbsp;příkazech `jekyll serve` a&nbsp;`jekyll build`, osobně **už** mi nefungují (no jo, Mac). Místo nich fungují tyhle nové a&nbsp;dlouhé přes Bundler. Protože je budete používat s&nbsp;Jekyllem nejčastěji, doporučuji si na ně napsat [aliasy](https://www.linuxexpres.cz/praxe/bash-7-dil), třeba:
```
alias jeks="bundle exec jekyll serve"
alias jekb="bundle exec jekyll build"
```

## Struktura
Počáteční struktura složek a souborů může vypadat následovně:
```
.
├── _config.yml
├── _includes
|   ├── footer.html
|   └── header.html
├── _layouts
|   └── post.html
|   └── page.html
├── _posts
|   ├── 2016-04-20-first-post.md
|   └── 2016-06-06-another-scientific-article.md
├── _data
|   └── partners.yml
├── _site
├── assets
|   └── _sass
├── Gemfile
└── index.md
```

Shrnutí, k čemu jednotlivé věci jsou:
- **_config.yml**: konfigurační soubor s nastavením stránek
- **_includes**: znovu použitelné menší části šablony
- **_layouts**: šablony, které určují rozložení kódu
- **_posts**: články do blogu
- **_data**: soubory s daty přístupné kdekoli v aplikaci
- **assets**: složka pro css, javascript, obrázky a další
- **Gemfile**: info o potřebných Ruby gems (dependencies)
- **index.md**: soubor s informacemi a obsahem pro hlavní stránku

Existují ještě další složky a&nbsp;soubory, které se dají využít nebo byly využívány kdysi. Třeba `_drafts` je podobně jako `_posts` pro příspěvky do blogu, ale pouze pro návrhy - rozepsané články, které ještě nechceme publikovat. Nebo pro všechny stránky a&nbsp;podstránky můžeme vytvořit složku `_pages` (konkrétně soubory *404.md*, *contact.md* a&nbsp;tak).

Běžná struktura Jekyllu ve verzi 2 vypadala trochu jinak, například složky `css`, `js` a&nbsp;`images` přímo v&nbsp;kořenovém adresáři. Aktuální verze 3.3 zavádí nově složku `assets`, kde kromě běžného CSS a&nbsp;JavaScriuptu můžeme mít rovnou i&nbsp;SASS a&nbsp;CoffeeScript. Více informací o&nbsp;verzi 3.3 a&nbsp;budoucích updatech naleznete na [Jekyll News](https://jekyllrb.com/news/).

## Konfigurační soubor
`_config.yml` je [YAML soubor](http://docs.ansible.com/ansible/YAMLSyntax.html) uchovávající všechny základní informace jako třeba název a&nbsp;popis stránky, URL, autora, základní layout, nastavení pluginů a&nbsp;další. Při lokálním Jekyll serveru (`bundle exec jekyll serve`) se soubor `_config.yml` načte pouze na začátku, proto po každé změně v&nbsp;souboru musíme server shodit (`Ctrl + C`) a&nbsp;následně zase nahodit.

Nastavit se toho dá mnoho, ale na začátek se hodí vědět o&nbsp;polích `include` a&nbsp;`exclude`, kde specifikujeme, které soubory nebo&nbsp;adresáře má a&nbsp;nemá Jekyll brát v&nbsp;potaz při generaci. Většinou chceme něco jako:
```
include: [_pages, .htaccess]
exclude: [README.md, Gemfile, Gemfile.lock, _site, src, vendor, CNAME, LICENSE, Rakefile, Guardfile, .sass-cache, .DS_Store]
```

Pro Jekyll bylo vytvořeno nemálo [šablon](http://jekyllthemes.org), které můžeme jednoduše nastavit v&nbsp;`_config.yml` a&nbsp;nechat Jekyll, ať sám stáhne vše potřebné. Často tak můžeme využívat layouty a&nbsp;includy definované šablonou a&nbsp;z&nbsp;vlastního projektu úplně vypustit složky `_layouts`, `_includes` a&nbsp;hlavně `assets`.

### Závěr
Ukázali jsme si o čem je Jekyll, jak ho nainstalovat, nahodit lokální server a&nbsp;k&nbsp;čemu zhruba slouží jednotlivé soubory a&nbsp;složky. Příště zkusíme vytvořit a&nbsp;upravit nějaký smyšlený Jekyll web.

---

Užitečné: [Configuration Settings](https://jekyllrb.com/docs/configuration/), [Jekyll Themes](http://jekyllthemes.org)
