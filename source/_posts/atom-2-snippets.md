---
title: "Atom 02: Snippets"
tags: [ide, atom, workflow]
homepage: false
date: 2017-01-08 13:13:37
---
V minulém článku jsem skončili u&nbsp;klávesových zkratek a&nbsp;tvoření vlastních zkratek v souboru *keymap.cson*. V tomhle navazujícím článku se podíváme na tzv. **snippety**.

<!-- more -->

## Snippets
Díky snippetům můžeme ze zkratky (z pár písmen) rychle vytvořit často potřebné kusy kódu. Něco jako Emmet? Přesně. Můžeme napsat třeba `ieie` a&nbsp;klávesou `Tab` expandovat kód na následující strukturu:

```
if (true) {

} else if (false) {

} else {

}
```

Další je například snippet pro otravné `$this->`, které najdeme snad v každém PHP projektu (pokud jde o&nbsp;OOP). Stačí nám napsat `t`, ťuknout na klávesu `Tab` a&nbsp;nechat si vygenerovat výše zmíněné, včetně dolaru a&nbsp;šipky. Tento snippet je ale uložený pod zkratkou `this`. Samotné `t` bude fungovat pouze v případě, že není jiný snippet, který by tomuhle konkuroval.

## Vlastní snippety
Atom má už v základu několik užitečný snippetů, ale také nám umožňuje přidat své vlastní. Všechny uživatelské custom snippets můžeme vytvářet v souboru `snippets.cson`, který je ve skryté složce `~/.atom/` (alespoň na Macu).

První musíme specifikovat, v jakém prostředí (scope) chceme snippet používat. Například pro HTML můžeme použít jednoduše *.text.html* a&nbsp;pro JavaScript zase *.source.js*. V CSON syntaxi by pár snippetů pro JavaScript mohlo vypadat následovně:

```
'.source.js':
  'console log':
    'prefix': 'log'
    'body': 'console.log(${1:"hello world"});$2'
  'if, else':
    'prefix': 'ie'
    'body': """
      if (${1:true}) {
        $2
      } else {
        $3
      }
    """
```

Jak vidíme, způsob zápisu není nijak složitý. První specifikujeme název snippetu (např. `'console log'`), poté `'prefix'` určuje spouštěcí zkratku, kterou budeme používat, a&nbsp;do `'body'` zadáme vygenerovaný kód.


## Proměnné ve snippetech?
Ne přímo proměnné tak, jak je známe, ale ve snippetech se dají specifikovat počáteční hodnoty a&nbsp;pozice kurzoru. K tomu slouží znak dolaru. Z předchozího příkladu by mělo být vše docela jasné, pro jistotu ale vysvětlivka:

- **$1** značí počáteční místo, kde se nachází kurzor po rozbalení snippetu
- **$2** značí hned další místo, na které kurzor přeskočí po zmáčknutí klávesy `Tab`
- **$3** třetí místo atd.
- **${1:true}** je způsob, jak na místo dosadit pořáteční (default) hodnotu


## Snippets scope
Scope můžeme přeložit jako rozsah, prostor, rámec, prostředí... používat budu ale jednoduše **scope**. Scopem tady zjednodušeně rozumíme typ souborů, ve kterém námi definované snippety mají fungovat.

Scope seznam pro oblíbené jazyky:
- HTML: .text.html.basic
- PHP: .text.html.php
- JavaScript: .source.js
- CSS: .source.css
- Sass: .source.sass
- Java: .source.java
- Ruby: .text.html.erb
- Python: .source.python
- Plain text: .text.plain

Nejjednoduší způsob, jak zjistit scope, je otevřít *Settings -> Packages* a&nbsp;hledat podle slova "language". Vyberete konkrétní jazyk (třeba "language-php") a&nbsp;hned pod prvním nadpisem je malým písmem napsaný scope (viz. fialový rámeček na obrázku)

![Atom: Snippet scope](http://emper.cz/img/blog/atom-scope.png "Snippet scope")

Pro některé jazyky nebo typy souborů nainstalovaný balíček nenajdeme, ale scope se dá vždycky nějak domyslet. Například pro oblíbený Markdown můžeme snippety psát do scope `.text.md` nebo `.text.markdown`. V tomhle případě bude ale fungovat i&nbsp;obyčený `.text.plain`.

---

Užitečné: [Documentation](https://atom.io/docs), [Snippets (Flight Manual)](http://flight-manual.atom.io/using-atom/sections/snippets/), [Working With Snippets](https://webdesign.tutsplus.com/tutorials/atom-in-60-seconds-working-with-snippets--cms-26875)
