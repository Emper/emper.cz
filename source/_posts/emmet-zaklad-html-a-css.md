---
title: "Emmet: Základ psaní HTML a CSS"
tags: [workflow]
homepage: true
date: 2016-09-28 00:39:56
---
Prý, že takový dnešní kodér či programátor webových aplikací už čisté HTML skoro nevidí. To ale není tak docela pravda. Většina z&nbsp;nich se s HTML určitě setkává častěji, než by sami chtěli. Není žádná novinka, že psaní čistého HTML je docela časově náročná práce. Co nám pomůže a&nbsp;zrychlí práci při psaní zlověstného HTML? Emmet.

<!-- more -->
[Emmet (kdysi Zen Coding)](http://emmet.io) je sada nástrojů do vašeho oblíbeného textového editoru, které umožní psát daleko méně a&nbsp;získat daleko více. Emmet rapidně urychluje psaní HTML a&nbsp;CSS. Je napsaný v javascriptu a&nbsp;momentálně je dostupný pro více jak 30 vývojových prostředí, včetně těch oblíbených jako Sublime Text, Atom, Eclipse, NetBeans, Brackets, PSPad nebo Notepad++, ale dokonce i&nbsp;pro Vim nebo Emacs.

## Workflow
Emmet se chová tak, že napíšete CSS selektor, třeba jen název elementu bez špičatých závorek `< >`, stisknete tabulátor a&nbsp;Emmet z&nbsp;výrazu udělá validní HTML element nebo několik elementů. Napíšeme například `img` stiskneme tabulátor a&nbsp;vygeneruje se `<img src="" alt="" />` s&nbsp;tím, že kurzor je automaticky mezi prvními uvozovkami, abychom hned mohli vyplnit hodnoty. Někdy tabulátor může blbnout, alternativa je stisknout `Ctrl + E` nebo `Cmd + Shift + E`.

## CSS příklady
Při stylování CSS se doplňují názvy vlastností i&nbsp;jejich hodnoty podle definovaných zkratek. Například `mb ` => `margin-bottom: ` nebo `w15 ` => `width: 15px;`.

Pro CSS je postup docela jednoduchý, proto uvedu pouze několik zajímavých příkladů, ze kterých by mělo být vše jasné:
```
w100    =>  width: 100px;
w100p   =>  width: 100%;
mih60   =>  min-height: 60px;
mah80   =>  max-height: 80px;
m20-30  =>  margin: 20px 30px;
m5e20   =>  margin: 5em 20px;
m10-a   =>  margin: 10px auto;
posr    =>  position: relative;
db      =>  display: block;
ib      =>  display: inline-block;
ov:v    =>  overflow: visible;
td      =>  text-decoration: none;
bgc#f   =>  background-color: #fff;
lh2e!   =>  line-height: 2em !important;
```

Jde vidět, že některé vlastnosti mají nějakou výchozí hodnotu. Hodnota se také může zadávat přes dvojtečku (např. tedy `w:100`, `pos:r`, `d:b` atd), ale ve většině případů to funguje i&nbsp;bez ní.

## HTML příklady
Díky Emmetu můžeme vygenerovat několik desítek řádků kódu jen z&nbsp;několika znaků. Máme přitom možnost specifikovat všechno možné, od tříd a&nbsp;id, přes potomky a&nbsp;sousedy (child and sibling), až po  vlastní atributy nebo obsah tagu. Většina znaků se používá podobně jako v&nbsp;CSS selektorech. Nespecifikovaný element se rozbalí jako `<div>`, viz. poslední řádek prvního příkladu. Nebo jako `<span>`, pokud se nacházíme uvnitř odstavce, odkazu atd.

Jednoduché příklady na třídy a&nbsp;identifikátory:
```
a              =>  <a href=""></a>
a.link         =>  <a href="" class="link"></a>
h1#headline    =>  <h1 id="headline"></h1>
p.center.fat   =>  <p class="center fat"></p>
h3#title.link  =>  <h3 id="title" class="link"></h3>
br             =>  <br />
.container     =>  <div class="container"></div>
```

Potomci se stejně jako v CSS značí `>`. Příklad na potomky, násobení a&nbsp;seskupení (druhý příkaz je ekvivalentní s&nbsp;`ul>li*3>a`, ale doporučuje se seskupovat závorkami):
```
p>img  =>  <p><img src="" alt="" /></p>

ul>(li>a)*3  =>
  <ul>
    <li><a href=""></a></li>
    <li><a href=""></a></li>
    <li><a href=""></a></li>
  </ul>
```

Atributy elementů se píší do hranatých závorek `[ ]`:
```
a[title="Download"]  =>  <a href="" title="Download"></a>

tr>td[rowspan=2 colspan=3]*2  =>
  <tr>
    <td rowspan="2" colspan="3"></td>
    <td rowspan="2" colspan="3"></td>
  </tr>
```

Delší příklad na vše zmíněné a&nbsp;navíc znak `+`, tedy sousedící element:
```
.container>(nav>ul>li*2>a)+#content+footer>p  =>
  <div class="container">
    <nav>
      <ul>
        <li><a href=""></a></li>
        <li><a href=""></a></li>
      </ul>
    </nav>
    <div id="content"></div>
    <footer>
      <p></p>
    </footer>
  </div>
```

Pokud některé elementy nebo skupiny násobíme, máme k&nbsp;dispozici čítač iterace pod znakem dolaru `$`:
```
ul>li.item-$*2  =>
  <ul>
    <li class="item-1"></li>
    <li class="item-2"></li>
  </ul>
```

Navíc se dá specifikovat i&nbsp;obsah elementu do složených závorek `{ }`:
```
button.btn{Here}  =>  <button class="btn">Here</button>

ul>(li>a>i.icon-$+{Link $})*3  =>
  <ul>
    <li><a href=""><i class="icon-1"></i>Link 1</a></li>
    <li><a href=""><i class="icon-2"></i>Link 2</a></li>
    <li><a href=""><i class="icon-3"></i>Link 3</a></li>
  </ul>
```

Výše zmíněné konstrukce se chovají podle určitých pravidel. Kromě nich ale existují ještě další. Například znak vykřičníku `!` vygeneruje základní strukturu HTML5 stránky (stejně jako `html:5`):
```
!  =>
  <!DOCTYPE html>
  <html lang="en">
  <head>
    <meta charset="UTF-8" />
    <title>Document</title>
  </head>
  <body>

  </body>
  </html>
```

A na závěr série atypických zkratek:
```
.          =>  <div class=""></div>
#          =>  <div id=""></div>
a:link     =>  <a href="http://"></a>
a:mail     =>  <a href="mailto:"></a>
str        =>  <strong></strong>
tr+        =>  <tr><td></td></tr>
link       =>  <link rel="stylesheet" href="" />
link:css   =>  <link rel="stylesheet" href="style.css" />
script:src =>  <script src=""></script>
form:get   =>  <form action="" method="get"></form>
form:post  =>  <form action="" method="post"></form>
inp        =>  <input type="text" name="" id="" />
input:p    =>  <input type="password" name="" id="" />
input:c    =>  <input type="checkbox" name="" id="" />
input:s    =>  <input type="submit" value="" />
btn:s      =>  <button type="submit"></button>
btn:d      =>  <button disabled="disabled"></button>
```

## Navíc lorem ipsum generátor
V Emmetu je také ukrytá schopnost generovat lipsum. Stačí napsat `loremN`, kde `N` je libovolný počet slov. Například `lorem120` vygeneruje 120 slov výplňového textu rozloženého do několika vět. `lorem` bez čísla vygeneruje 30 slov.

### Závěr
[Emmet](http://emmet.io) se rozhodně vyplatí používat. Urychlí zdlouhavé vypisování tagů, přitom je jednoduchý a&nbsp;velmi rychle se do toho dostanete. Bez Emmetu už ani ránu! Pokud nechcete hned instalovat plugin do editoru, můžete si to první vyzkoušet na [CodePen](http://codepen.io) nebo [JSFiddle](https://jsfiddle.net).

---

Užitečné: [Emmet Cheat Sheet](http://docs.emmet.io/cheat-sheet/), [Emmet Documentation](http://docs.emmet.io/)
