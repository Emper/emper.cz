---
title: Klávesové zkratky na Macu
tags: [mac os, workflow]
homepage: false
date: 2016-10-11 11:25:19
---
Práce na Mac OS X je bez klávesových zkratek pěkně na nic. Každý uživatel Macu (ale i&nbsp;jiných systémů) by si používání zkratek měl osvojit a&nbsp;zrychlit tak svou workflow.

<!-- more -->
Základní chování můžeme podrobně prozkoumat a&nbsp;nebo přenastavit v&nbsp;*Předvolby systému -> Klávesnice -> Zkratky*. Nalezneme zde jak Macovské zkratky, tak zkratky jednotlivých aplikací. Navíc můžeme definovat i&nbsp;vlastní nové zkratky, ale pouze pokud požadovanou akci určitá aplikace už nabízí. Nemůžeme zde třeba vytvořit zkratku na otevření určité aplikace, to už je totiž složitější.

Níže naleznete úžasný **seznam nejpoužívanějších a&nbsp;nejužitečnějších zkratek** (podle mě). Pokud by vás to zajímalo více do hloubky, koukněte na budoucí návod na **vytvoření zkratky pro spouštění aplikace** (konkrétně Terminálu).

## Mac OS a práce s textem
- `Cmd + Tab`: změna aplikace
- `Cmd + F`: hledat v textu
- `Cmd + A`: vybrat veškerý text
- `Cmd + B`: tučné písmo
- `Cmd + C`: kopírovat

... Ok, takhle to asi dál nepůjde. Tohle zná každý, jdeme dál.

## Většina aplikací
- `Cmd + N`: nové okno
- `Cmd + T`: nový panel (tab)
- `Cmd + W`: zavřít panel
- `Cmd + Q`: quit, vypnutí aplikace
- `Cmd + S`: uložit / uložit jako
- `Cmd + O`: otevřít soubor
- `Cmd + Z`: zpět (undo)
- `Cmd + Shift + Z`: opakovat akci (redo)
- `Cmd + M`: minimalizovat okno
- `Cmd + H`: skrýt (schovat) aplikaci
- `Cmd + Alt + M`: minimalizovat všechna okna aplikace
- `Cmd + Alt + H`: skrýt všechny ostatní aplikace

Některé ze zkratek výše platí obecně pro všechny aplikace. Zbytek alespoň pro většinu aplikací, například Finder, Safari, Chrome, FireFox, Opera, Sublime Text, Atom, Brackets a&nbsp;další.

## Finder
- `Cmd + Shift + C`: tento počítač
- `Cmd + Shift + H`: domovská složka uživatele
- `Cmd + Shift + A`: aplikace (`U` pro utility)
- `Cmd + Shift + D`: plocha
- `Cmd + I`: informace o souboru / složce
- `Cmd + D`: duplikovat vybrané soubory
- `Cmd + Shift + N`: vytvořit novou složku
- `Cmd + Shift + G`: otevřít složku
- `Cmd + Shift + Delete`: vysypat koš (delete = backspace)
- `Cmd + Alt + Shift + Delete`: bez varování vysypat koš
- `Cmd + 1`: zobrazení - ikony
- `Cmd + 2`: zobrazení - seznam
- `Cmd + 3`: zobrazení - sloupce
- `Cmd + Ctrl + 1`: seřadit podle názvu
- `Cmd + Ctrl + 2`: seřadit podle druhu (atd...)

## Safari
- `Cmd + Left`: zpět v historii (také `Cmd + [`)
- `Cmd + Right`: vpřed v historii (také `Cmd + ]`)
- `Cmd + Shift + Left`: přesun na panel vlevo
- `Cmd + Shift + Right`: přesun na panel vpravo
- `Cmd + 1`: první panel okna
- `Cmd + 2`: druhý panel okna (atd...)
- `Cmd + Shift + B`: zobrazit / skrýt řádek oblíbených
- `Cmd + Shift + L`: zobrazit / skrýt boční panel
- `Cmd + Shift + N`: nové anonymní okno
- `Cmd + R`: refresh, znovu načíst stránku
- `Cmd + L`: zaměření na řádek s adresou
- `Cmd + D`: přidat stránku do záložek
- `Cmd + I`: poslat stránku mailem
- `Cmd + Alt + I`: inspektor webu
- `Cmd + Alt + C`: chybová konzola
- `Cmd + Alt + R`: zobrazit jako zařízení s jinak velkou obrazovkou

## Kalkulačka
- `Cmd + 1`: základní kalkulačka
- `Cmd + 2`: vědecká kalkulačka
- `Cmd + 3`: programátorská kalkulačka

## Mac OS X extra
- `Shift + Space`: hledání ve Spotlightu
- `Cmd + Shift + 3`: screenshot celé obrazovky (3 = š)
- `Cmd + Shift + 4`: screenshot vybrané části (4 = č)
- `Alt + Shift + F1/F2`: přesnější změna jasu obrazovky
- `Alt + Shift + F4/F5`: přesnější změna podsvícení klávesnice
- `Alt + Shift + F11/F12`: přesnější změna hlasitosti
- `Ctrl + ⏏ (vysunutí média)`: okno, zda restartovat/spát/vypnout
- `Cmd + Alt + ⏏`: režim spánku
- `Cmd + Ctrl + ⏏`: ukončí aplikace a restartuje počítač
- `Cmd + Alt + D`: zobrazit / skrýt Dock
- `Cmd + Alt + Esc`: vynutit ukončení aplikací
- `Cmd + Alt + Shift + Esc`: vynutit ukončení aktivní aplikace

---

Užitečné: [Klávesové zkratky Macu](https://support.apple.com/cs-cz/HT201236)
